/* 
//codigo para observar en consola
var canvas = document.getElementById("myCanvas");
var contexr = canvas.getContext('2d');

canvas.addEventListener("mousemove", function(evt){
    function getMousePos (canvas, evt){
        return{
            x:evt.clientX,
            y:evt.clientY
        }
    }
    var mousePos = getMousePos(canvas, evt);
    var message = "Mouse position: " + mousePos.x +", " +mousePos.y;
    console.log(message);
});
*/

//codigo visto desde la pagina
window.onload = function (){
    document.onmousemove = function(e){
        var contenedor = document.getElementById("contenedor");
        contenedor.innerHTML = "x: " + e.pageX + " " + "y: " + e.pageY;
    }
}

//mostrar tecla presionada
var text= document.getElementById("text");

text.addEventListener("keypress", function(){
    console.log("tecla presionada: ", this.value);
})