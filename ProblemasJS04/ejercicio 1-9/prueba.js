/*
//Ejercicio 1. Dado el siguiente objeto y arreglo:
var anObject = {
    foo: 'bar',
    lenght: 'interesting',
    '0': 'zero!',
    '1': 'one!'
};
var anArray = ['zero.', 'one.'];

//¿Cual es el resultado del siguiente código y por qué?
console.log(anArray[0], anObject[0]);           //Regresa zero. zero!
console.log(anArray[1], anObject[1]);           //Regresa one. one!
console.log(anArray.lenght, anObject.lenght);   //Regresa undefined "interesting"
console.log(anArray.foo, anObject.foo);         //Regresa undefined "bar"

console.log(typeof anArray == 'object', typeof anObject == 'object');   //Regresa true true
console.log(anArray instanceof Object, anObject instanceof Object);     //Regresa true true
console.log(anArray instanceof Array, anObject instanceof Array);       //Regresa true false
console.log(Array.isArray(anArray), Array.isArray(anObject));           //Regresa true false
*/
/*
//Ejercicio 2. Dado el siguiente objeto:
var obj = {
    a: "hello",
    b: "this is",
    c: "javascript!",
};
//Que metodo del objeto 'Object' puedo usar para que me dé el siguiente resultado.
console.log(obj); //["hello", "his is", "javascript!"]
*/

/*
//Ejercicio 3. Crear una funcion para imprimir en consola la siguiente serie de números.
//0
//2
//4
//...
//98
for (var i = 0; i<=98; i=i+2){
    document.write(i + "<br>")
}
*/


/*
//Ejercicio 4. Dado el siguiente código.
let zero = 0;
function multiply(x) { 
    return x * 2;
    }
    function add(a = 1 + zero, b = a, c = b + a, d = multiply(c)) {
        console.log((a + b +c), d);
    }

//¿Cuál es el resultado de las siguientes sentencias?
add(1);             //Regresa 4 4 
add(3);             //Regresa 12 12
add(2, 7);          //Regresa 18 18  
add(1, 2, 5);       //Regresa 8 10
add(1, 2, 5, 10);   //Regresa 8 10
*/

/*
//5. Dado el siguiente código:
class MyClass{
    constructor() {
        this.names_ = [];
    }

    set name(value) {
        this.names_.push(value);
    }

    get name() {
        return this.names_[this.names_.length - 1];
    }
}

const myClassInstance = new MyClass();
myClassInstance.name = 'Joe';
myClassInstance.name = 'Bob';

//¿Cuál es el resultado de ejecutar las siguientes sentencias y por qué?
console.log(myClassInstance.name); //regresa "Bob"
console.log(myClassInstance.names_); //regresa un array de 2 elementos "Joe" y "Bob"
*/

/*
//Ejercicio 6. Dado el siguiente código:
const classInstance = new class {
    get prop() {
        return 5;
    }
};

//¿Cuál es el resultado de ejecutar las siguientes sentesncias y por qué?:
classInstance.prop = 10;
console.log(classInstance.prop); //regresa 5
*/

/*
//Ejercicio 7. Dado el siguiente código:
class Queue {
    constructor () {
        const list = [];
        this.enqueue = function (type) {
            list.push(type);
            return type;
        };
        this.dequeue = function () {
            return list.shift();
        };
    }
}

//¿Cuál es el resultado de ejercutar las siguientes sentencias y por qué?:
var q = new Queue;
q.enqueue(9);
q.enqueue(8);
q.enqueue(7);

console.log(q.enqueue());       //regresa undefined
console.log(q.enqueue());       //regresa undefined
console.log(q.enqueue());       //regresa undefined
console.log(q);                 //regresa Queue {enqueue: ƒ, dequeue: ƒ}
console.log(Object.keys(q));    //regresa ["enqueue", "dequeue"]
*/

/*
//Ejercicio 8. Dada la siguiente clase:
class Person{
    #firstname;
    #lastname;
    constructor(firstname, lastname) {
        this.firstname = firstname;
        this.lastname = lastname
    }
    get nombreCompleto(){
        return`${this.firstname}, ${this.lastname}`;
    }

}
//Define los métodos correspondientes para ejecutar el siguiente código:
let person = new Person('John', 'Doe');
console.log(person.nombreCompleto);

person.firstname = 'Foo';
person.lastname = 'Bar';

console.log(person.nombreCompleto);

//Nota: las propiedades firstname y lastname deben no ser accesibles desde el exterior.
*/


//Ejercicio 9. Dado el siguiente código HTML:
/* 
<div id="post-102">
        <p>I like Confirm modals</p>
        <a data-deletepost="post-102">Delete post</a>
    </div>
    <div id="post-103">
        <p>That´s way too cool!</p>
        <a data-deletepost="post-103">Delete post</a>
    </div>

var deleteBtn = document.querySelectorAll("[data-deletepost]");
*/


